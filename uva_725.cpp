// Problems 725

#include <iostream>
using namespace std;

int main() {
    int n;
    while (cin >> n) {
        if (n != 0) {
            bool sol = false;
            for (int fghij = 1234; fghij <= 98765/n; fghij++) {
                int abcde = fghij*n;
                int tmp, used = (fghij<10000);
                tmp = abcde; while (tmp) {used |= 1 << (tmp%10); tmp /=10;}
                tmp = fghij; while (tmp) {used |= 1 << (tmp%10); tmp /= 10;}
                if (used == (1<<10) - 1) { 
                    if (fghij >= 10000) cout << abcde << " / " << fghij << " = " << n << endl;
                    else cout << abcde << " / " << 0 << fghij << " = " << n << endl;
                    sol = true;
                }
            }
            if (sol == false) {
                cout << "There are no solutions for " << n << "." << endl;
            }
            cout << endl;
        }
    }
    return 0;
}