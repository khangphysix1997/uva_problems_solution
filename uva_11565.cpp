//problem 11565

#include <iostream>
using namespace std;

int main() {
    int a, b, c, n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a >> b >> c;
        bool sol = false; int x,y,z;
        for (x = -100; x <= 100 && !sol; x++) {
            if (x*x <= c) {
                for (y = -100; y <= 100 && !sol; y++) {
                    if (x*x + y*y <= c) {
                        for (z = -100; z <= 100 && !sol; z++) {
                            if (x + y +z == a && x*y*z == b && x*x + y*y +z*z == c && z != x && z!=y) {
                                cout << x << " " << y << " " << z << endl;
                                sol = true;
                            }
                        }
                    }
                } 
            }
        }
        if (sol == false) cout << "No solution." << endl;
    }
    return 0;
}