#include <iostream>
using namespace std;

int T, n, arr[1005], countt = 0, sum = 0;

int main() {
	cin >> T;
	while (T--) {
		cin >> n;
		for (int i = 1; i <= n; i++) cin >> arr[i];
		sum = 0;
		for (int i = 2; i <= n; i++) {
			countt = 0;
			for (int j = 1; j < i; j++) {
				if (arr[j] <= arr[i]) countt++;
			}
			sum += countt;
		}
		cout << sum << endl;
	}
	return 0;
}