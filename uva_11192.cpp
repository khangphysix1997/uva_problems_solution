#include <bits/stdc++.h>
using namespace std;

char arr[15][15];
int n, ans = 0, a = 0;
bitset<30> rw, ld, rd;

void backtrack(int c) {
	if (c == n) {ans++; return;}
	for (int r = 0; r < n; r++) {
		if (arr[r][c] != '*' && !rw[r] && !ld[r-c + n - 1] && !rd[r+c]) {
			rw[r] = ld[r-c+n-1] = rd[r+c] = true;
			backtrack(c+1);
			rw[r] = ld[r-c+n-1] = rd[r+c] = false;
		}
	}
}

int main() {
	while (cin >> n) {
		if (n == 0) return 0;
		a++;
		ans = 0;
		for (int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				cin >> arr[i][j];
			}
		}
		backtrack(0);
		cout <<"Case " << a << ": " << ans << endl;
	}
	return 0;
}