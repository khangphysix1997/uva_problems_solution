#include <iostream>
using namespace std;

int T, n, arr[100005];

int main() {
	cin >> T;
	while (T--) {
		cin >> n;
		for (int i = 0; i < n; i++) cin >> arr[i];
		int l = -100000, lmax = -200000;
		for (int i = 0; i < n; i++) {
			if (l - arr[i] > lmax) lmax = l - arr[i];
			if (l < arr[i]) l = arr[i];
		}
		cout << lmax << endl;
	}
	return 0;
}