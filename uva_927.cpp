#include <bits/stdc++.h>
using namespace std;

long long T, j, arr[21], d, k;

int main() {
	cin >> T;
	while (T--) {
		cin >> j;
		for (int i = 0; i <= j; i++) cin >> arr[i];
		cin >> d >> k;
		long long a = 1;
		long long sum = a*d;
		while (sum < k) {
			a++;
			sum += a*d;
		}
		sum = 0;
		for (int i = 0; i <= j; i++) sum += arr[i]*pow(a,i);
		cout << sum << endl;
	}
	return 0;
}