#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
using namespace std;

int f, r, fr[100], x;
double maxx = 0.0;
vector<double> myvec;

int main() {
	while (cin >> f) {
		if (f == 0) return 0;
		cin >> r;
		myvec.clear();
		for (int i = 0; i < f; i++) cin >> fr[i];
		for (int i = 0; i < r; i++) {
			cin >> x;
			for (int i = 0; i < f; i++) {
				myvec.push_back((double)x/fr[i]);
			}
		}
		sort(myvec.begin(), myvec.end());
		maxx = 0.0;
		for (int i = 0; i < myvec.size()-1; i++) {
			if (myvec[i+1]/myvec[i] >= maxx) maxx = myvec[i+1]/myvec[i];
		}
		cout << setprecision(2) << fixed << maxx << endl;
	}
	return 0;
}